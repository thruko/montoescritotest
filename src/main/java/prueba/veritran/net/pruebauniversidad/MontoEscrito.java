package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MontoEscrito {
    static Map <Integer,String>parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) {

        MontoEscrito MontoEscrito = new MontoEscrito();
        MontoEscrito.getMontoEscrito(0);
        Scanner leer = new Scanner(System.in);
        Integer valor = 1;
        System.out.println("Bienvenido \n" +
                "------------------------------------------------------------- \n" +
                "la app muestra la parte textual de un número escrito \n" +
                "------------------------------------------------------------- \n");


        int x = 1;
        Scanner scanner = new Scanner(System.in);
        while (x >= 0) {
            try {
                System.out.println("Ingrese un numero para convertirlo a texto. \n"
                        + "Si ingresa un numero negativo el sistema se cierra.");
                x = scanner.nextInt();
                String texto = MontoEscrito.getMontoEscrito(x);
                System.out.println(texto);
            } catch (Exception e) {
                System.out.println("Ingresar numeros " + e);
                scanner.next();
            }
        }

    }
    public static String getMontoEscrito(Integer valor) {

        crearParseNumeros();
        String resultado = "";
        String resultado2 = "";
        if (valor < 0 || valor > 1000000) {
            return "El numero debe estar entre 0 y 1000000";
        } else {
            if (parseoNumeros.containsKey(valor)) {
                return parseoNumeros.get(valor);
            } else {
                int ciclo = 1;
                if (valor > 1000) {
                    ciclo = 2;
                }
                for (int i = 0; i < ciclo; i++) {
                    int unidad = valor % 10;
                    valor = valor / 10;
                    int unidad2 = valor % 10;
                    valor = valor / 10;
                    resultado = numeroDosCifras(unidad2, unidad);
                    int unidad3 = valor % 10;
                    valor = valor / 10;
                    if (unidad3 > 0) {
                        if (unidad3 * 100 == 100) {
                            resultado = parseoNumeros.get(unidad3 * 100) + "to " + resultado;
                        } else if (parseoNumeros.containsKey(unidad3 * 100)) {
                            resultado = parseoNumeros.get(unidad3 * 100) + " " + resultado;
                        } else if (unidad3 * 100 == 700) {
                            resultado = "setecientos " + resultado;
                        } else if (unidad3 * 100 == 900) {
                            resultado = "novecientos " + resultado;
                        } else {
                            resultado = parseoNumeros.get(unidad3) + "cientos " + resultado;
                        }
                    } else if (unidad2 > 0 && unidad > 0 && i == 1) {
                        resultado = numeroDosCifras(unidad2, unidad);
                    }else if(unidad2 == 0 && unidad > 0 && i == 1){
                        if (parseoNumeros.containsKey(unidad * 1000)) {
                            resultado = parseoNumeros.get(unidad * 1000);
                        }else{
                            resultado = parseoNumeros.get(unidad);
                        }
                    }else if(valor > 0){
                        int y = 0;
                    }else{
                        return resultado;
                    }
                    if (ciclo == 2 && i == 0) {
                        resultado2 = resultado;
                    }
                    if (ciclo == 2 && i == 1) {
                        if (unidad > 1) {
                            resultado = resultado + " mil " + resultado2;
                        }else{
                            resultado = resultado + " " + resultado2;
                        }

                    }
                }
            }
        }
        return resultado;










        }
    private static String numeroDosCifras(int unidad2, int unidad) {
        String resultado = "";
        if (parseoNumeros.containsKey((unidad2 * 10) + unidad)) {
            resultado = parseoNumeros.get((unidad2 * 10) + unidad);
        }else if (((unidad2 * 10) + unidad) > 15 && ((unidad2 * 10) + unidad) < 20) {
            resultado = "dieci" + parseoNumeros.get(unidad);
        } else if (((unidad2 * 10) + unidad) > 20 && ((unidad2 * 10) + unidad) < 30) {
            resultado = "veinti" + parseoNumeros.get(unidad);
        } else if (unidad2 == 0 && unidad == 0) {
            resultado = "";
        } else if (unidad2 == 0) {
            resultado = parseoNumeros.get(unidad);
        } else {
            resultado = parseoNumeros.get(unidad2 * 10) + " y " + parseoNumeros.get(unidad);
        }
        return resultado;
    }
    private static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}


